﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {

    public float delay = 6f;
    public float pause = 3f;
    public float speed = 0.2f;

    private Vector3 startPoint;
    private Vector3 endPoint;

    // Use this for initialization
    void Start () {
        this.startPoint = this.transform.position;

        GameObject end = this.transform.Find("EndPoint").gameObject;
        this.endPoint = end.transform.position;
        end.SetActive(false);

        StartCoroutine("Move");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Move() {
        yield return new WaitForSeconds(this.delay); // delay until movement begins

        while(true) {
            float t = 0f;
            while (t < 1f) { // move from start point to end point
                t += Time.deltaTime * speed;
                float tSmooth = Mathf.SmoothStep(0f, 1f, t);
                this.transform.position = Vector3.Lerp(this.startPoint, this.endPoint, tSmooth);
                yield return null;
            }

            yield return new WaitForSeconds(this.pause); // wait at end point

            t = 0f;
            while (t < 1f) { // move back to start point
                t += Time.deltaTime * this.speed;
                float tSmooth = Mathf.SmoothStep(0f, 1f, t);
                this.transform.position = Vector3.Lerp(this.endPoint, this.startPoint, tSmooth);
                yield return null;
            }

            yield return new WaitForSeconds(this.pause); // wait at start point
        }
    }

}
