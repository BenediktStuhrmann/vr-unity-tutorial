﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public float timer = 20;
    public Canvas gameOverlay;
    public Canvas endScreen;
    public AudioSource tickSound;
    public AudioSource startSound;
    public AudioSource finishSound;

    private int coinsCollected = 0;
    private Text scoreText;
    private Text timerText;
    private Text countdownText;
    private float timeLeft;
    private float countdownTimer = 3;
    private float countdown;
    private int coinsTotal;
    private ThirdPersonController characterControl;
    private bool gameIsOver = false;
    private bool gameRunning = false;

    // Use this for initialization
    void Start () {
        this.characterControl = FindObjectOfType<Player>().GetComponent<ThirdPersonController>();

        this.endScreen.enabled = false;
        this.scoreText = this.gameOverlay.transform.Find("Coins").GetComponent<Text>();
        this.timerText = this.gameOverlay.transform.Find("Timer").GetComponent<Text>();
        this.countdownText = this.gameOverlay.transform.Find("Countdown").GetComponent<Text>();

        this.timeLeft = this.timer;
        if (this.timerText != null) this.timerText.text = "Time left: " + this.timeLeft.ToString() + "s";
        this.countdown = this.countdownTimer;
        InvokeRepeating("ShowCountdown", 1, 1);
        Coin[] coins = FindObjectsOfType<Coin>();
        this.coinsTotal = coins.Length;
        if (this.scoreText != null) this.scoreText.text = "Coins: " + this.coinsCollected.ToString() + "/" + this.coinsTotal;
    }

    void CoinCollected(Coin coin) {
        if (coin != null)
        {
            this.coinsCollected++;  
            coin.gameObject.GetComponent<Renderer>().enabled = false;
            AudioSource sound = coin.gameObject.GetComponent<AudioSource>();
            sound.Play();
            Destroy(coin, sound.clip.length);
            if (this.scoreText != null) this.scoreText.text = "Coins: " + this.coinsCollected.ToString() + "/" + this.coinsTotal;
        }
    }

    void ShowCountdown() {
        if (gameIsOver) return;

        if (this.countdown == 0) this.StartGame();
        else {
            this.countdown--;
            this.countdownText.text = this.countdown.ToString();
        }
    }

    void StartGame() {
        if (!this.gameRunning) this.startSound.Play();
        this.gameRunning = true;
        this.characterControl.setMoveable(true);
        this.countdownText.text = "";
        this.UpdateTime();
    }

    void UpdateTime() {
        if (gameIsOver) return;

        if (this.timeLeft == 0) this.StopGame(false);
        else {
            this.timeLeft--;
            this.tickSound.Play();
        }
        if (this.timerText != null) this.timerText.text = "Time left: " + this.timeLeft.ToString() + "s";
    }

    void ReachedFinish() {
        this.finishSound.Play();
        this.StopGame(true);
    }

    void StopGame(bool finish) {
        this.gameIsOver = true;
        this.finishSound.Play();

        this.gameOverlay.enabled = false;
        this.endScreen.enabled = true;
        //this.characterControl.setMoveable(false);

        Text endMessage = this.endScreen.transform.Find("EndMessage").GetComponent<Text>();
        if (!finish) endMessage.text = "Game over!";
        else endMessage.text = "Level completed!";

        Text coins = this.endScreen.transform.Find("Coinamount").GetComponent<Text>();
        coins.text = "You collected " + this.coinsCollected.ToString() + "/" + this.coinsTotal + " coins";

        Text time = this.endScreen.transform.Find("Time left").GetComponent<Text>();
        time.text = "You had " + this.timeLeft.ToString() + " seconds left";

        Text points = this.endScreen.transform.Find("Points").GetComponent<Text>();
        points.text = "Points total: " + (this.timeLeft + this.coinsCollected);
    }
}
