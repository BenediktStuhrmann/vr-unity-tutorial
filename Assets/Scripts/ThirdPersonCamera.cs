﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public Transform target;
    public float cameraSensitivity = 160f;
    public float minDistance = 3f;
    public float maxDistance = 10f;
    public float zoomSpeed = 160f;
    public float minXAngle = -90f;
    public float maxXAngle = 90f;
    public float correctionSpeed = 10f;

    private bool lockPosition = false;
    private float xAngle;
    private float yAngle;
    private float currentDistance = 10f;
    private float targetDistance;
    private float homingSpeed = 3f;
    private bool isHoming = true;

    // Use this for initialization
    void Start() {
        if (target == null) {
            this.enabled = false;
        } else {
            this.xAngle = this.target.eulerAngles.x;
            this.yAngle = this.target.eulerAngles.y;
        }
        this.targetDistance = this.currentDistance;
    }

    // LateUpdate is called once per frame
    void LateUpdate() {
        // save axis input
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        float scrollWheel = Input.GetAxis("Mouse ScrollWheel") * 10;

        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Strafe") != 0)
            // if player moves, start homing
            this.isHoming = true;
        else
            this.isHoming = false;

        // camera rotation
        float lastXAngle = this.xAngle;
        if (Input.GetMouseButton(0)) {
            // allow to rotate camera x and y, when left mouse button is pressed
            this.xAngle += mouseY * Time.deltaTime * this.cameraSensitivity;
            this.yAngle += mouseX * Time.deltaTime * this.cameraSensitivity;
            this.isHoming = false;
        } else if (Input.GetMouseButton(1)) {
            // when right mouse buton is pressed, allow to rotate y only
            this.xAngle += mouseY * Time.deltaTime * this.cameraSensitivity;
            this.isHoming = false;
        }

        // camera homing
        if (this.isHoming) {
            // slowly reset rotation
            this.yAngle = Mathf.LerpAngle(this.yAngle, this.target.eulerAngles.y, this.homingSpeed * Time.deltaTime);

            if (Mathf.Abs(this.yAngle - this.target.eulerAngles.y) < 0.1f) {
                // camera reset completed
                this.isHoming = false;
                this.yAngle = this.target.eulerAngles.y;
            }
        }

        // check if camera collides with terrain
        float distanceCorrection = 0f;
        int terrainMask = 1 << LayerMask.NameToLayer("Terrain");

        RaycastHit hit = new RaycastHit();
        Vector3 direction = this.transform.position - this.target.position;
        if (Physics.SphereCast(this.target.position, 0.5f, direction, out hit, this.targetDistance, terrainMask))
            distanceCorrection = hit.distance - this.targetDistance;

        // camera distance to the player
        float distance = Mathf.SmoothStep(this.targetDistance, this.targetDistance - scrollWheel, this.zoomSpeed * Time.deltaTime);
        this.targetDistance = Mathf.Clamp(distance, this.minDistance, this.maxDistance);
        this.currentDistance = Mathf.Clamp(this.targetDistance + distanceCorrection, this.minDistance, this.maxDistance);

        this.xAngle = ClampAngle(this.xAngle, this.minXAngle, this.maxXAngle);

        // stop roation, when minDistance is reached (including distance correction)
        if (this.currentDistance == this.minDistance && distanceCorrection != 0 && mouseY > 0)
            this.xAngle = lastXAngle;

        // set camera position and rotation
        this.transform.rotation = Quaternion.Euler(-this.xAngle, this.yAngle, 0);

        if (this.lockPosition == true) this.transform.LookAt(this.target);
        else this.transform.position = this.target.position + this.transform.TransformDirection(new Vector3(0, 0, -this.currentDistance));
    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle > 360)       angle -= 360;
        else if (angle < -360) angle += 360;
        return Mathf.Clamp(angle, min, max);
    }

    public void LockPosition(bool lockPos)
    {
        this.lockPosition = lockPos;
    }

}