﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonController : MonoBehaviour {

    public float rotationSpeed = 200f;
    public float mouseSensitivity = 50f;
    public float walkingSpeed = 8f;
    public float strafeSpeed = 4f;
    public float jumpHeight = 60f;
    public float airSpeed = 4f;
    public float weight = 500f;

    private CharacterController characterController;
    private float rotationAngle;
    private Vector3 velocity = new Vector3(0, 0, 0);
    private bool moveable = false;

    // Use this for initialization
    void Start () {
        characterController = this.GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!moveable) return;

        float forward = Input.GetAxis("Vertical");
        float rotate = Input.GetAxis("Horizontal");
        float strafe = Input.GetAxis("Strafe");
        float mouseX = Input.GetAxis("Mouse X");

        if (Input.GetMouseButton(1)) {
            // when right mouse buton is pressed, allow to rotate the player
            rotate += mouseX * Time.deltaTime * this.mouseSensitivity;
        }

        Vector3 airVelocity = new Vector3();

        if (this.characterController.isGrounded) {
            // grounded

            if (Input.GetButton("Jump")) {
                // jump
                this.transform.parent = null;
                this.velocity.y += this.jumpHeight;
                this.characterController.gameObject.GetComponent<Player>().audios[0].Play();
            }
            else {
                // walk
                this.velocity = this.transform.right * strafe * this.strafeSpeed + this.transform.forward * forward * this.walkingSpeed;
            }

        } else {
            // mid air
            this.transform.parent = null;
            airVelocity = this.transform.right * strafe * this.strafeSpeed + this.transform.forward * forward * this.airSpeed;
        }

        // apply gravity
        this.velocity.y -= this.weight * Time.deltaTime;

        // apply rotation
        this.transform.Rotate(Vector3.up, rotate * this.rotationSpeed * Time.deltaTime);
        // move character
        characterController.Move((this.velocity + airVelocity) * Time.deltaTime);
	}

    public void setMoveable(bool canMove) {
        this.moveable = canMove;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit) {
        if (hit.gameObject.CompareTag("Platform") && (this.characterController.collisionFlags & CollisionFlags.Below) != 0)
            this.transform.parent = hit.transform;
    }

}
