﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolZone : MonoBehaviour {

    public CharacterController enemyController;
    public float speed = 3f;
    public AudioSource alertSound;

    private float radius;
    private Vector3 direction;
    private Vector3 velocity = Vector3.zero;
    private GameObject intruder = null;

	// Use this for initialization
	void Start () {
        this.radius = GetComponent<SphereCollider>().radius;
        this.direction = this.enemyController.transform.forward;

        // place enemy in the middle of the zone
        float enemyY = this.transform.position.y + (this.enemyController.height / 2);
        this.enemyController.transform.position = new Vector3(this.transform.position.x, enemyY, this.transform.position.z);
    }
	
	// Update is called once per frame
	void Update () {
        if (this.intruder != null) this.alertSound.Play();

        // difference between enemy and center of zone
        Vector3 diff = Vector3.Exclude(Vector3.up, this.enemyController.transform.position) - Vector3.Exclude(Vector3.up, this.transform.position);
        float distance = diff.magnitude;
        if (distance >= this.radius) { // if enemy reached end of zone, ...
            if (this.intruder == null) { // ... turn him randomly towards the zone if there's no player
                this.direction = Quaternion.AngleAxis((Random.value - 0.5f) * 160, Vector3.up) * -diff; // between -80 and 80
            } else { // ... turn him towards the player if there's one
                this.direction = this.intruder.transform.position - this.enemyController.transform.position; // towards the player
            }
            Quaternion rotation = Quaternion.FromToRotation(this.enemyController.transform.forward, this.direction);
            this.enemyController.transform.rotation = rotation * this.enemyController.transform.rotation;
        }

        this.velocity = this.enemyController.transform.forward * this.speed;
        this.enemyController.Move(this.velocity * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            this.intruder = other.gameObject;
            this.enemyController.gameObject.SendMessage("Prepare", SendMessageOptions.DontRequireReceiver);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")) {
            this.intruder = null;
            this.enemyController.gameObject.SendMessage("Idle", SendMessageOptions.DontRequireReceiver);
        }
    }
}
