﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private GameManager gameManager;
    public AudioSource[] audios;

    // Use this for initialization
    void Start () {
        this.gameManager = FindObjectOfType<GameManager>();
        this.audios = GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Coin") && other.GetComponent<Coin>() != null)
            gameManager.SendMessage("CoinCollected", other.GetComponent<Coin>());

        if (other.CompareTag("Finish"))
            gameManager.SendMessage("ReachedFinish");

        if (other.CompareTag("Void"))
            this.Perish();
    }

    private void Perish() {
        this.audios[1].Play();

        FindObjectOfType<ThirdPersonCamera>().LockPosition(true);

        gameManager.SendMessage("StopGame", false);
    }
}
