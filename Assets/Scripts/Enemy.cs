﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private AudioSource sound;

	// Use this for initialization
	void Start () {
        this.sound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) other.gameObject.SendMessage("Perish");
    }

    public void Prepare() {
        this.sound.Play();
    }

    public void Idle() {
        this.sound.Stop();
    }
}
